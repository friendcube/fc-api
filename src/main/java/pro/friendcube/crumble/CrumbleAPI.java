package pro.friendcube.crumble;

import org.bukkit.plugin.Plugin;

public class CrumbleAPI {
    private Plugin plugin;

    public CrumbleAPI(Plugin plugin) {
        this.plugin = plugin;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }
}
