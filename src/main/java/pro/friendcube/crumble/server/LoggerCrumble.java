package pro.friendcube.crumble.server;

import org.apache.commons.lang.exception.ExceptionUtils;
import pro.friendcube.crumble.CrumbleAPI;

import java.util.logging.Level;

public class LoggerCrumble {
    private CrumbleAPI crumble;

    /**
     * Class Constructor of CrumbleAPI Logger.
     *
     * @param instance Parent instance of CrumbleAPI.
     */
    public LoggerCrumble(CrumbleAPI instance) {
        this.crumble = instance;
    }

    /**
     * @return {@link CrumbleAPI} Returns parent instance of CrumbleAPI.
     */
    public CrumbleAPI getCrumble() {
        return crumble;
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void severe(String message) {
        crumble.getPlugin().getLogger().log(Level.SEVERE, message);
    }

    /**
     * @param exception a generic error exception to be stacktrace generated
     */
    public void severe(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.SEVERE, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void warning(String message) {
        crumble.getPlugin().getLogger().log(Level.WARNING, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void warning(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.WARNING, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void info(String message) {
        crumble.getPlugin().getLogger().log(Level.INFO, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void info(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.INFO, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void config(String message) {
        crumble.getPlugin().getLogger().log(Level.CONFIG, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void config(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.CONFIG, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void fine(String message) {
        crumble.getPlugin().getLogger().log(Level.FINE, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void fine(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.FINE, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void finer(String message) {
        crumble.getPlugin().getLogger().log(Level.FINER, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void finer(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.FINER, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void finest(String message) {
        crumble.getPlugin().getLogger().log(Level.FINEST, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void finest(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.FINEST, ExceptionUtils.getStackTrace(exception));
    }

    /**
     * @param message Message to be sent to Server Console.
     */
    public void all(String message) {
        crumble.getPlugin().getLogger().log(Level.ALL, message);
    }

    /**
     * @param exception Exception to be sent to Server Console.
     */
    public void all(Exception exception) {
        crumble.getPlugin().getLogger().log(Level.ALL, ExceptionUtils.getStackTrace(exception));
    }

}
